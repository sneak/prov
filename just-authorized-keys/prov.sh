#!/bin/bash

URL="https://dl.sneak.cloud/0000/authorized_keys"

function install_authorized_keys () {
    mkdir -p /root/.ssh
    curl -sL $URL > /root/.ssh/authorized_keys.new
    if [[ $(wc -l /root/.ssh/authorized_keys.new | awk '{print $1}') -gt 0 ]]; then
        mv /root/.ssh/authorized_keys.new /root/.ssh/authorized_keys
    else
        rm -f /root/.ssh/authorized_keys.new
        echo "failed to download authorized_keys" > /dev/stderr
        return
    fi
    chmod 700 /root/.ssh
    chmod 600 /root/.ssh/authorized_keys
    echo success
    exit 0
}

install_authorized_keys
